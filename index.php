<?php
    require ('Frog.php');
    require ('Ape.php');
    

    $sheep = new Animal("shaun");

    echo $sheep->name; // "shaun"
    echo "<br>";
    echo $sheep->legs; // 2
    echo "<br>";
    var_dump($sheep->cold_blooded); // false
    echo "<br>";

    $sungokong = new Ape("kera sakti");
    $sungokong->yell();// "Auooo"
    echo "<br>";
    $kodok = new Frog("buduk");
    $kodok->jump() ; // "hop hop"
    echo "<br>";    
    echo $kodok->legs;
    
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>

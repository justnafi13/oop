<?php
    require_once ('Animal.php');

    class Frog extends Animal {
        
        function __construct(){
            $this->legs = 4;
        }

        function jump() {
            echo "hop hop";
        }
    }
?>
<?php
    class Animal {
        public $cold_blooded = false;
        
        function __construct($name) {
            $this->legs = 2;
            $this->name = $name;
        }
    }
?>